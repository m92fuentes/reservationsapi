using Microsoft.AspNetCore.Mvc;
using WebApplication.Utils.Models;

namespace WebApplication.Utils.Extensions
{
  public static class ControllerBaseExtensions
  {
    public static bool IsOrderingValid<T>(this ControllerBase controllerBase, Ordering ordering)
    {
      if (ordering.IsOrdering())
      {
        var type = typeof(T);
        var orderingColumn = ordering.GetOrderingColumn();
        return type.GetProperty(orderingColumn) != null;
      }

      return true;
    }
  }
}
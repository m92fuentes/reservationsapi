using System.Linq;
using System.Runtime.Serialization;
using WebApplication.Utils.Extensions;

namespace WebApplication.Utils.Models
{
  [DataContract]
  public class Ordering
  {
    [DataMember] public string column { get; set; } = "";

  }

  public static class OrderingExtensions
  {
    public static bool IsOrdering(this Ordering ordering)
    {
      return ordering.column != "";
    }

    public static bool IsDescending(this Ordering ordering)
    {
      return ordering.column.StartsWith('-');
    }

    public static string GetOrderingColumn(this Ordering ordering)
    {
      var column = ordering.column;
      if (ordering.IsDescending())
        column = ordering.column.Substring(1);
      return $"{column[0].ToString().ToUpper()}{column.Substring(1)}";
    }

    public static IQueryable SetOrderQuery<T>(this Ordering ordering, IQueryable<T> query)
    {
      if (ordering.IsOrdering())
      {
        var order = ordering.GetOrderingColumn();
        return query.OrderBy( order, ordering.IsDescending());
      }
      return query;
    }
  }
}
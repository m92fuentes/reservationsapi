using System.Runtime.Serialization;

namespace WebApplication.Utils.Models
{
 
  [DataContract]
  public class Paging
  {
    private static int defaultPerPage = 5;
    
    [DataMember]
    public int PerPage { get; set; } = defaultPerPage;
      
    [DataMember]
    public int Total { get; set; }
    [DataMember]
    public int Page { get; set; } 
  }

  public static class PagingExtensions
  {
    public static int OffSet(this Paging paging)
    {
      return paging.PerPage * paging.Page;
    }

    public static Paging CreatePaging(this Paging paging, int total)
    {
      return new Paging()
      {
        PerPage = paging.PerPage,
        Total = total,
        Page = paging.Page
      };
    }
  }
}
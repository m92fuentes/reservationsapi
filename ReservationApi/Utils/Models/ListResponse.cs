using System.Runtime.Serialization;

namespace WebApplication.Utils.Models
{
  [DataContract]
  public class ListFilter
  {
    [DataMember] public Ordering Ordering { get; set; } = new Ordering();

    [DataMember] public Paging Paging { get; set; } = new Paging();
  }
}
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication.Data;
using WebApplication.Models;
using WebApplication.Utils.Extensions;
using WebApplication.Utils.Models;

namespace WebApplication.Controllers
{
  [Route("api/reservations")]
  [ApiController]
  public class ReservationsController : ControllerBase
  {
    private readonly ReservationContext _reservationContext;

    public ReservationsController(ReservationContext reservationContext)
    {
      this._reservationContext = reservationContext;
    }

    [HttpGet()]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<PagedResponse<Reservation>>> GetReservationsList([FromQuery] ListFilter filters)
    {
      var paging = filters.Paging;
      var ordering = filters.Ordering;

      if (!this.IsOrderingValid<Reservation>(ordering))
        return BadRequest($"Column \"{ordering.GetOrderingColumn()}\" doesn't exist");
      
      var count = await this._reservationContext.Reservations.CountAsync();
      var query = this._reservationContext.Reservations
        .Skip(paging.OffSet())
        .Take(paging.PerPage);

      query = (IQueryable<Reservation>)ordering.SetOrderQuery(query);
      var results = await query.ToListAsync();

      return new PagedResponse<Reservation>()
      {
        Data = results,
        Ordering = filters.Ordering,
        Paging = paging.CreatePaging(count),
      };
    }
    
    [HttpGet("byContact/{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<PagedResponse<Reservation>>> GetReservationsByContactList(int id, [FromQuery] ListFilter filters)
    {
      var paging = filters.Paging;
      var ordering = filters.Ordering;

      if (!this.IsOrderingValid<Reservation>(ordering))
        return BadRequest($"Column \"{ordering.GetOrderingColumn()}\" doesn't exist");

      var contactById = await this._reservationContext.Contacts.FindAsync(id);
      if (contactById == null)
        return NotFound($"There is no contact with id: {id}");

      var R = this._reservationContext.Reservations;
      var joinedQuery = from reservation in R where reservation.ContactId == id select reservation; 
      
      var count = await joinedQuery.CountAsync();
      var query = joinedQuery
        .Skip(paging.OffSet())
        .Take(paging.PerPage);

      query = (IQueryable<Reservation>)ordering.SetOrderQuery(query);
      var results = await query.ToListAsync();

      return new PagedResponse<Reservation>()
      {
        Data = results,
        Ordering = filters.Ordering,
        Paging = paging.CreatePaging(count),
      };
    }

    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<Reservation>> GetReservationById(int id)
    {
      var reservationById = await _reservationContext.Reservations
        .Include(p => p.Contact)
        .Where(p => p.ReservationId == id)
        .SingleOrDefaultAsync();
      
      if (reservationById == null)
        return NotFound($"There is no Reservation with id: {id}");
      return Ok(reservationById);
    }

    [HttpPost()]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<Reservation>> CreateReservation(Reservation reservation)
    {
      var errorMessage = await IsValid(reservation);
      if (errorMessage != "")
        return BadRequest(errorMessage);

      await _reservationContext.Reservations.AddAsync(reservation);
      await _reservationContext.SaveChangesAsync();

      return CreatedAtAction(nameof(CreateReservation), reservation);
    }

    [HttpPut("{id}")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<Reservation>> UpdateReservation(int id, Reservation reservation)
    {
      var reservationById = await _reservationContext.Reservations.FindAsync(id);
      if (reservationById == null)
        return NotFound($"There is no Reservation with id: {id}");
      var errorMessage = await IsValid(reservation);

      if (errorMessage != "")
        return BadRequest(errorMessage);

      reservationById.ContactId = reservation.ContactId;
      reservationById.Description = reservation.Description;
      reservationById.Favorite = reservation.Favorite;
      reservationById.Name = reservation.Name;
      reservationById.CreationTime = reservation.CreationTime;
      reservationById.Ranking = reservation.Ranking;

      await _reservationContext.SaveChangesAsync();
      return Ok(reservationById);
    }

    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> DeleteReservation(int id)
    {
      var reservationById = await _reservationContext.Reservations.FindAsync(id);
      if (reservationById == null)
        return NotFound($"There is no Reservation with id: {id}");

      this._reservationContext.Reservations.Remove(reservationById);
      await this._reservationContext.SaveChangesAsync();
      return NoContent();
    }
    
    

    private async Task<string> IsValid(Reservation reservation)
    {
      var contact = await _reservationContext.Contacts.FindAsync(reservation.ContactId);

      if (contact == null)
        return "Linked contact doesn't exist";
      return "";
    }
  }
}
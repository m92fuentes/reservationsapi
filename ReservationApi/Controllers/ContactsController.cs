using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication.Data;
using WebApplication.Models;
using WebApplication.Utils.Extensions;
using WebApplication.Utils.Models;

namespace WebApplication.Controllers
{
  [Route("api/contacts")]
  [ApiController]
  public class ContactsController: ControllerBase
  {
    private readonly ReservationContext _reservationContext;

    public ContactsController(ReservationContext reservationContext)
    {
      this._reservationContext = reservationContext;
    }

    [HttpGet("byName")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<Contact>> GetContactByName([FromQuery] string name = "")
    {
      var contact = await this.GetContactByNameHelper(name);

      if (contact == null)
      {
        return NotFound();
      }
      
      return contact;
    }

    [HttpPost()]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult> CreateContact(Contact contact)
    {
      var validMessage = IsValid(contact);
      if (validMessage != "")
        return BadRequest(validMessage);
      
      var contactFromDb = await this.GetContactByNameHelper(contact.Name);

      if (contactFromDb != null)
      {
        return Conflict($"Already exists a contact with name {contact.Name}");
      }
      
      await this._reservationContext.Contacts.AddAsync(contact);
      await this._reservationContext.SaveChangesAsync();
      return CreatedAtAction(nameof(CreateContact), contact);
    }

    [HttpPut("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status409Conflict)]
    public async Task<ActionResult> UpdateContact(int id, Contact contact)
    {
      var contactById = await _reservationContext.Contacts.FindAsync(id);
      if (contactById == null)
        return NotFound($"There is no contact with id: {id}");
      var contactByName = await this.GetContactByNameHelper(contact.Name);
      if (contactByName != null && contactByName.ContactId != id)
        return Conflict($"There is already a contact with name: {contact.Name}");      
      var validMessage = IsValid(contact);
      if (validMessage != "")
        return BadRequest(validMessage);

      contactById.Name = contact.Name;
      contactById.PhoneNumber = contact.PhoneNumber;
      contactById.BirthDate = contact.BirthDate;
      contactById.ContactType = contact.ContactType;
      
      await _reservationContext.SaveChangesAsync();

      return this.Ok(contactById);
    }

    [HttpDelete("{id}")]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> DeleteContact(int id)
    {
      var contact = await this._reservationContext.Contacts.FindAsync(id);

      if (contact == null)
        return NotFound();

      this._reservationContext.Contacts.Remove(contact);
      await this._reservationContext.SaveChangesAsync();
      return NoContent();
    }

    [HttpGet()]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<ActionResult<PagedResponse<Contact>>> ListContacts([FromQuery]ListFilter filters)
    {
      var paging = filters.Paging;
      var ordering = filters.Ordering;

      if (!this.IsOrderingValid<Contact>(ordering))
        return BadRequest($"Column \"{ordering.GetOrderingColumn()}\" doesn't exist");

      var count = await this._reservationContext.Contacts.CountAsync();
      var query = this._reservationContext.Contacts
        .Skip(paging.OffSet())
        .Take(paging.PerPage);

      query = (IQueryable<Contact>)ordering.SetOrderQuery(query);
      var results = await query.ToListAsync();

      return new PagedResponse<Contact>()
      {
        Data = results,
        Ordering = filters.Ordering,
        Paging = paging.CreatePaging(count),
      };
    }

    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult> GetContactById(int id)
    {
      var contactById = await _reservationContext.Contacts.FindAsync(id);
      if (contactById == null)
        return NotFound($"There is no contact with id: {id}");
      return Ok(contactById);
    }

    private Task<Contact> GetContactByNameHelper(string name)
    {
      return this._reservationContext.Contacts.FromSqlInterpolated(
        $"select * from get_contact_by_name({name})"
      ).FirstOrDefaultAsync();
    }

    private string IsValid(Contact contact)
    {
      if (contact.Name == null)
      {
        return "Contact name must have a value";
      }

      //todo: More validations could be added

      return "";
    }
  }
}
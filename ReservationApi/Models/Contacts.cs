using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace WebApplication.Models
{
    public enum ContactType
    {
        ContactType1,
        ContactType2,
        ContactType3,
    }
    
    public class Contact
    {
        public int ContactId { get; set; }
        
        public string Name { get; set; }
        public ContactType ContactType { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime BirthDate { get; set; }
        public ICollection<Reservation> Reservations { get; set; }
    }
}